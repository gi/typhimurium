Nabil-Fareed Alikhan, Zhemin Zhou, Martin J. Sergeant, Mark Achtman. "A genomic overview of the population structure of _Salmonella_" PLoS genetics. 2018. 14(4):e1007261.
