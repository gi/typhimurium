# Typhimurium

5,712 genomes from the _Salmonella_ serovar Typhimurium obtained from EnteroBase 04/2019:

-  Nabil-Fareed Alikhan, Zhemin Zhou, Martin J. Sergeant, Mark Achtman. "A genomic overview of the population structure of _Salmonella_" PLoS genetics. 2018. 14(4):e1007261.

For 6,000 randomly selected genomes from a total of 19,237 that were annotated as serovar Typhimurium in EnteroBase (on April 09, 2019), 5,712 assemblies were available and have been downloaded. Not all of these assemblies are available on EnteroBase anymore and bulk download on command line is currently not possible. Here, we provide a copy of the data for reasons of reproducibility of experiments that have been performed on that data meanwhile, e.g. in

- Tizian Schulz, Roland Wittler, Sven Rahmann, Faraz Hach, Jens Stoye. "Detecting High Scoring Local Alignments in Pangenome Graphs" Bioinformatics. (2021)
- Andreas Rempel, Roland Wittler "SANS serif: alignment-free, whole-genome based phylogenetic reconstruction" Bioinformatics. 2021.


This provision of the data has been permitted by the EnteroBase development team. We thank the team for their valuable help.

When using this data, cite the EnteroBase publication:

-  Nabil-Fareed Alikhan, Zhemin Zhou, Martin J. Sergeant, Mark Achtman. "A genomic overview of the population structure of _Salmonella_" PLoS genetics. 2018. 14(4):e1007261.
